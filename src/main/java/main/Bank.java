package main;

import java.util.ArrayList;

public class Bank implements BankOperation {

    ArrayList<Account> accounts = new ArrayList<Account>();

    @Override
    public int createAccount() {

        Account account = new Account();
        accounts.add(account);

        return account.getId();
    }
    @Override
    public int deleteAccount(int accountNumber) {

        for(Account a : accounts){
            if(a.getId() == accountNumber){
                int delAccBalance = a.getBalance();
                accounts.remove(a);
                return delAccBalance;
            }
        }

        return ACCOUNT_NOT_EXISTS;
    }
    @Override
    public boolean deposit(int accountNumber, int amount) {

        for(Account a : accounts){
            if(a.getId() == accountNumber && amount > 0) {
                a.setBalance(a.getBalance() + amount);
                return true;
            }
        }

        return false;

    }
    @Override
    public boolean withdraw(int accountNumber, int amount) {

        for(Account a : accounts){
            if(a.getId() == accountNumber && amount > 0 && (a.getBalance() > amount)) {
                a.setBalance(a.getBalance() - amount);
                return true;
            }
        }

        return false;

    }
    @Override
    public boolean transfer(int fromAccount, int toAccount, int amount) {

        Account srcAccount = null;
        Account destAccount = null;

        for(Account a : accounts){
            if(a.getId() == fromAccount) {
                srcAccount = a;
            } else if(a.getId() == toAccount){
                destAccount = a;
            }
        }

        if(srcAccount.getBalance() > amount){
            srcAccount.setBalance(srcAccount.getBalance() - amount);
            destAccount.setBalance(destAccount.getBalance() + amount);
            return true;
        }

        return false;

    }
    @Override
    public int accountBalance(int accountNumber) {

        for(Account a : accounts){
            if(a.getId() == accountNumber) {
                int balance = a.getBalance();
                return balance;
            }
        }

        return ACCOUNT_NOT_EXISTS;

    }
    @Override
    public int sumAccountsBalance() {

        int sum = 0;
        for(Account a : accounts){
            sum += a.getBalance();
        }

        return sum;

    }
}
